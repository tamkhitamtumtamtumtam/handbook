---
title: Change Management
description: Support Operations documentation page for change management
canonical_path: "/handbook/support/readiness/operations/docs/change_management"
---

The exact process for change management can vary from project to project and
item to item, so when in doubt, it is best to refer to the specific
documentation for the item in question.
